<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(){
        Schema::create('wells', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->boolean('state');
            $table->string('type');
            $table->float('depth');
            $table->float('level_min');
            $table->float('level_min_relaunch');
            $table->float('volume');
            $table->string('stat_frequency');
            $table->string('photo');
            $table->string('address');
            $table->boolean('state_presostat');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('wells');
    }
};
