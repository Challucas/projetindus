<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(){
        Schema::create('stats', function (Blueprint $table) {
            $table->id();$table->date('date_stat');
            $table->float('level_water');
            $table->boolean('is_consolidated');
            $table->unsignedBigInteger('well_id');
            $table->foreign('well_id')->references('id')->on('wells');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('stats');
    }
};
