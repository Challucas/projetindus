<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = new User();
        $user->firstname = "toto";
        $user->lastname = "otot";
        $user->email = "toto@gmail.com";
        $user->password = Hash::make('secret');
        $user->save();
    }
}
